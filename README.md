# CS451 Project

[Link to slides](https://drive.google.com/file/d/1f6Qsnu7A7YKf9I_buW6YwRaMo6eowle3/view?usp=sharing)

The only file that matters is
`subreddit_cleaning/src/main/scala/ca/uwaterloo/cs451/cleaning/LinReg.scala`.
It contains all data processing and machine learning.
https://www.kaggle.com/leukipp/reddit-finance-data will need to be downloaded
and unzipped in the root of this project for it to work.

```
cd subreddit_cleaning/
mvn clean package
spark-2.4.7-bin-hadoop2.7/bin/spark-submit --class ca.uwaterloo.cs451.cleaning.LinReg target/assignments-1.0.jar
```

Contact apregasz@uwaterloo.ca if building the project fails.

## Sample for failed sentiment analysis

Sentiment Analysis:

```
(2021-01-01 04,NEUTRAL,GME to the moon 🚀🚀)
(2021-01-04 19,NEUTRAL,You NEED to see this about GME 🚀🚀🚀🚀🚀🚀)
(2021-01-04 22,NEUTRAL,Short Squeeze Incoming 🚀🚀🚀🚀🚀🚀🚀)
(2021-01-05 10,NEGATIVE,THIS CONVINCED ME TO ALL IN 💰GME (EXTREME PUMP COMING))
(2021-01-06 01,NEUTRAL,You already know what we must do brothers and sisters. Submit a complaint. I have done m
y part.)
(2021-01-06 13,NEGATIVE,ICR conference (11th Jan))
(2021-01-06 22,NEGATIVE,Hey guys! We have a free discord channel that's pretty active and we're all high on GME
)
(2021-01-10 21,POSITIVE,GME is FINALLY going to the moon, this technical analysis looks very nice 🚀🚀🚀)
(2021-01-11 12,NEUTRAL,Ryan Cohen appointed to board!!!!?)
(2021-01-11 12,NEGATIVE,Holly f*ck, our GME rollercoaster will break off and Moon!)
(2021-01-11 14,NEGATIVE,BUCKLE YOUR SEATBELTS OR YOURE GONNA FALL OFF THE TENDIE TRAIN 🚀🚀🚀)
(2021-01-11 16,NEUTRAL,Ryan Cohen not being paid???)
(2021-01-12 16,NEGATIVE,Low Volume the day after Cohen essentially takes control of the board)
(2021-01-12 17,NEUTRAL,questions from a potential investor about gamestop's business in the long run)
(2021-01-13 11,NEGATIVE,The reason why GME isn't exploding upwards)
(2021-01-13 11,NEUTRAL,Cramer showing interest in GME 🤩)
(2021-01-13 15,NEUTRAL,Hold the line! 420.69)
(2021-01-13 15,POSITIVE,new investor here, advice welcome)
(2021-01-13 16,NEUTRAL,You retards know what's up)
(2021-01-13 16,NEUTRAL,You guys were right, retarded, but right.)
(2021-01-13 17,NEGATIVE,Anyone hear that Point72 is short GME?)
(2021-01-13 17,NEGATIVE,WTF is this dip to punish people who jump on late? $3500 in GME, $1000 in PLTR - first
time be gentle)
(2021-01-13 23,NEUTRAL,Will GME maintain its current price?)
(2021-01-14 00,NEUTRAL,BUY CALLS FOR JAN 15 or Stock)
(2021-01-14 01,NEGATIVE,Me finding this group yesterday then me waking up today.)
(2021-01-14 02,NEGATIVE,:) short squeeze not done yet bois don't sell 💎👐)
(2021-01-14 02,NEUTRAL,GME YOLO 🚀🚀🚀🚀🚀🚀)
(2021-01-14 04,NOT_UNDERSTOOD,🚀)
(2021-01-14 05,NEUTRAL,Seeking for Opinion)
(2021-01-14 14,NEGATIVE,GME IS A SELL)
(2021-01-14 18,NEUTRAL,Question regarding my 4/16 $24 calls)
(2021-01-14 20,NEGATIVE,Should I buy more GME?)
(2021-01-14 21,NEUTRAL,Time to take profits?)
(2021-01-14 23,NEUTRAL,🚀 Question 🚀)
(2021-01-15 00,NEGATIVE,Where's our leader tonight?)
(2021-01-15 01,NEGATIVE,alright dont downvote me to the absolute pits of hell for this)
(2021-01-15 02,NEUTRAL,Short Squeeze)
(2021-01-15 08,NEUTRAL,Should i wake up early or set a order)
(2021-01-15 15,NEGATIVE,GME Keep dipping and someone sold 810k shares)
(2021-01-15 15,NEUTRAL,DIP BABY DIP. DIP! DIP! DIP!)
(2021-01-15 15,NEGATIVE,Why is wsb deleting gme posts?)
(2021-01-15 15,NEUTRAL,GME Analysis)
(2021-01-15 17,NEUTRAL,Buy now 🚀🚀)
(2021-01-15 19,NEGATIVE,THE SHORT SQUEEZE HAS ONLY BEGUN, we are going to the moon - and this is why🚀)
(2021-01-15 19,NEGATIVE,THE SHORT SQUEEZE HAS ONLY BEGUN, we are going to the moon - and this is why🚀)
(2021-01-15 19,NEGATIVE,Is next Wednesday too late to buy GME?)
(2021-01-15 21,NEGATIVE,Sold 2/3 of my GME position and put it in EOSE)
(2021-01-15 21,NEUTRAL,I’m doing my bit)
(2021-01-15 23,NEUTRAL,Let’s go! 🚀🚀🚀🚀🚀)
(2021-01-16 01,POSITIVE,"""We"" beat melvin")
(2021-01-16 03,POSITIVE,Today's view and this past Week view, it's all about perspective.)
(2021-01-16 16,NEUTRAL,How much do you set your sell limit to?)
(2021-01-16 18,NEGATIVE,GME Short Squeeze Will 10X And Make HISTORY - Here's Exactly Why)
(2021-01-17 15,NEUTRAL,GME bull community unite)
(2021-01-17 18,NEGATIVE,Whenever people tell me GME is overpriced.)
(2021-01-17 21,POSITIVE,Still worth it?)
(2021-01-17 23,NEUTRAL,Will I Have Time Tuesday Premarket To Get In On This Ride?)
(2021-01-18 02,NEUTRAL,How would GME's board benefit from a short squeeze?)
(2021-01-18 02,VERY_POSITIVE,Ryan Cohen is a brilliant guy, he made the perfect move with one little small mist
ake, he bought $GME!)
(2021-01-18 02,NEUTRAL,Some Fundamental GME Numbers)
(2021-01-18 02,POSITIVE,Stock Chart as of 01/17/2021 as a technical trader sees it)
(2021-01-18 10,NEUTRAL,A GameStop owned digital marketplace that can compete with Steam)
(2021-01-18 16,NEUTRAL,Friendly reminder)
(2021-01-18 18,NEUTRAL,Short Interest continues to climb (squeeze not done))
(2021-01-18 18,NEGATIVE,Bearish propaganda since 2020)
(2021-01-18 21,NEGATIVE,I’m probably retarded, but can someone confirm)
(2021-01-18 23,NEUTRAL,brokers desperate to borrow shares)
(2021-01-18 23,NEGATIVE,Do I need to set a sell limit?)
(2021-01-19 01,POSITIVE,Prepare for onslaught.)
(2021-01-19 02,NEUTRAL,Comparison Of Volkswagen Short Squeeze And GME)
(2021-01-19 05,NEGATIVE,If you don’t match my tat game, you gme DD is trash)
(2021-01-19 05,NEGATIVE,If you don’t have atleast 1 cohen tat, your gme DD is trash)
(2021-01-19 09,NEUTRAL,Premarket - $38.31 USD)
(2021-01-19 09,NEGATIVE,$GME Premarket 16%+ as of 19-1-2021 🥳)
(2021-01-19 13,NEUTRAL,What buy price on opening?)
(2021-01-19 13,NEUTRAL,MAKE GME GREAT AGAIN)
(2021-01-19 14,NEGATIVE,give them no shares. hold the line)
(2021-01-19 15,NEUTRAL,Down down down)
(2021-01-19 15,NEUTRAL,What dickhole is selling 93,000 shares??)
(2021-01-19 17,NEGATIVE,Reddit Hug of Death)
(2021-01-19 19,NEUTRAL,Propaganda film request)
(2021-01-19 22,NEGATIVE,$GME downgraded me from level 3 options after going all in GME shares 😕)
(2021-01-20 02,NEUTRAL,Any suggestions?)
(2021-01-20 11,NEGATIVE,$GME 1.45m shares. Not selling. Go longs!)
(2021-01-20 12,NEUTRAL,shares)
(2021-01-20 16,POSITIVE,I fully trust the forecasting ability of someone with this level of situational awarene
ss 🌈🐻)
(2021-01-20 17,NEUTRAL,Rocket baby)
(2021-01-20 17,NEUTRAL,Can‘t bear it anymore!)
(2021-01-20 21,NEUTRAL,💎👐 got in)
(2021-01-21 00,NEUTRAL,Greetings GME Gang)
(2021-01-21 01,NEUTRAL,Help Make Q4 Great)
(2021-01-21 05,NEUTRAL,GME 🌙🌙🌙🚀🚀🚀 gang gang!!)
(2021-01-21 05,NEUTRAL,I’d who else would enjoy this lmao)
(2021-01-21 08,NEUTRAL,LFG :))
(2021-01-21 09,NEUTRAL,WallStreetBets Bullied Me Into Buying $GME (GameStop))
(2021-01-21 11,NEGATIVE,Remember lads, scared money don't make money.)
(2021-01-21 14,NEGATIVE,I'm not selling!)
(2021-01-21 16,NEUTRAL,How do we know when the squeeze has happened?)
(2021-01-21 17,NEUTRAL,WHERE ARE THE FUCKING REINFORCEMENTS)
(2021-01-21 17,POSITIVE,Picked up more!)
```
