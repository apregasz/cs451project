import yfinance as yf
import os


FILE_PATH = 'gme-stocks'

data = yf.download("GME", start='2020-01-01', end='2021-04-16', interval='60m')
if os.path.exists(FILE_PATH):
    os.remove(FILE_PATH)

data.to_csv(FILE_PATH)