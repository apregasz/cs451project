//package ca.uwaterloo.cs451.cleaning
//
//import org.apache.spark.sql.SparkSession
//
//object Adam {
//  def clean_subreddit(spark: SparkSession, name: String, keywords: Array[String]) : org.apache.spark.rdd.RDD[(String, Float)] = {
//    spark.read.format("csv").option("header", "true").load(s"../${name}/submissions_reddit.csv")
//      .selectExpr("created", "title")
//      .filter(_.getAs("created") != null)
//      .filter(_.getAs[String]("created").matches("^\\d\\d\\d\\d-\\d\\d-\\d\\d \\d\\d.*"))
//      .rdd
//      .map(row => (row.getAs[String]("created").slice(0, 13), row.getAs[String]("title")))
//      .map(row => {
//        val title = row._2.toLowerCase()
//        var contains = false
//        for (keyword <- keywords) {
//          if (title.contains(keyword)) {
//            contains = true
//          }
//        }
//        (row._1, (if (contains) 1 else 0, 1))
//      })
//      .reduceByKey((p1, p2) => {
//        (p1._1 + p2._1, p1._2 + p2._2)
//      })
//      .map(tuple => (tuple._1, tuple._2._1.toFloat / tuple._2._2.toFloat))
//  }
//
//  def main(args: Array[String]) {
//    val spark = SparkSession.builder.appName("CS451 Project: Subreddit cleaning").getOrCreate()
//    val keywords = Array("gme", "gamestop")
//
//    val finance = clean_subreddit(spark, "finance", keywords)
//    val financialindependence = clean_subreddit(spark, "financialindependence", keywords)
//    val forex = clean_subreddit(spark, "forex", keywords)
//    val gme = clean_subreddit(spark, "gme", keywords)
//    val investing = clean_subreddit(spark, "investing", keywords)
//    val options = clean_subreddit(spark, "options", keywords)
//    val pennystocks = clean_subreddit(spark, "pennystocks", keywords)
//    val personalfinance = clean_subreddit(spark, "personalfinance", keywords)
//    val robinhood = clean_subreddit(spark, "robinhood", keywords)
//    val robinhoodpennystocks = clean_subreddit(spark, "robinhoodpennystocks", keywords)
//    val securityanalysis = clean_subreddit(spark, "securityanalysis", keywords)
//    val stockmarket = clean_subreddit(spark, "stockmarket", keywords)
//    val stocks = clean_subreddit(spark, "stocks", keywords)
//    val wallstreetbets = clean_subreddit(spark, "wallstreetbets", keywords)
//
//    // TODO join the subreddit data into:
//    // (date, (float, float, ..., float))
//    // TODO join this with the stock data into:
//    // (date, (float, float, ..., float, 1 or 0))
//
//    gme.take(100).foreach(println(_))
//    spark.stop()
//  }
//}
