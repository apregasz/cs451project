// package ca.uwaterloo.cs451.cleaning
// 
// import org.apache.spark.sql.SparkSession
// import scala.collection.JavaConverters._
// import java.util.Properties
// import com.vdurmont.emoji.EmojiParser
// 
// import java.util.Properties
// 
// import edu.stanford.nlp.ling.CoreAnnotations
// import edu.stanford.nlp.neural.rnn.RNNCoreAnnotations
// import edu.stanford.nlp.pipeline.StanfordCoreNLP
// import edu.stanford.nlp.sentiment.SentimentCoreAnnotations
// 
// import scala.collection.mutable.ListBuffer
// import scala.collection.JavaConversions._
// 
// import org.apache.hadoop.conf.Configuration
// import org.apache.hadoop.fs.{FileSystem, Path}
// object SentimentAnalysisUtils {
// 
//   val nlpProps = {
//     val props = new Properties()
//     props.setProperty("annotators", "tokenize, ssplit, pos, lemma, parse, sentiment")
//     props
//   }
// 
//   def detectSentiment(message: String): SENTIMENT_TYPE = {
// 
//     val pipeline = new StanfordCoreNLP(nlpProps)
// 
//     val annotation = pipeline.process(message)
//     var sentiments: ListBuffer[Double] = ListBuffer()
//     var sizes: ListBuffer[Int] = ListBuffer()
// 
//     var longest = 0
//     var mainSentiment = 0
// 
//     for (sentence <- annotation.get(classOf[CoreAnnotations.SentencesAnnotation])) {
//       val tree = sentence.get(classOf[SentimentCoreAnnotations.AnnotatedTree])
//       val sentiment = RNNCoreAnnotations.getPredictedClass(tree)
//       val partText = sentence.toString
// 
//       if (partText.length() > longest) {
//         mainSentiment = sentiment
//         longest = partText.length()
//       }
// 
//       sentiments += sentiment.toDouble
//       sizes += partText.length
// 
//       // println("debug: " + sentiment)
//       // println("size: " + partText.length)
// 
//     }
// 
//     val averageSentiment:Double = {
//       if(sentiments.size > 0) sentiments.sum / sentiments.size
//       else -1
//     }
// 
//     val weightedSentiments = (sentiments, sizes).zipped.map((sentiment, size) => sentiment * size)
//     var weightedSentiment = weightedSentiments.sum / (sizes.fold(0)(_ + _))
// 
//     if(sentiments.size == 0) {
//       mainSentiment = -1
//       weightedSentiment = -1
//     }
// 
// 
//     // println("debug: main: " + mainSentiment)
//     // println("debug: avg: " + averageSentiment)
//     // println("debug: weighted: " + weightedSentiment)
// 
//     /*
//      0 -> very negative
//      1 -> negative
//      2 -> neutral
//      3 -> positive
//      4 -> very positive
//      */
//     weightedSentiment match {
//       case s if s <= 0.0 => NOT_UNDERSTOOD
//       case s if s < 1.0 => VERY_NEGATIVE
//       case s if s < 2.0 => NEGATIVE
//       case s if s < 3.0 => NEUTRAL
//       case s if s < 4.0 => POSITIVE
//       case s if s < 5.0 => VERY_POSITIVE
//       case s if s > 5.0 => NOT_UNDERSTOOD
//     }
// 
//   }
// 
//   trait SENTIMENT_TYPE
//   case object VERY_NEGATIVE extends SENTIMENT_TYPE
//   case object NEGATIVE extends SENTIMENT_TYPE
//   case object NEUTRAL extends SENTIMENT_TYPE
//   case object POSITIVE extends SENTIMENT_TYPE
//   case object VERY_POSITIVE extends SENTIMENT_TYPE
//   case object NOT_UNDERSTOOD extends SENTIMENT_TYPE
// 
// }
//   // spark-submit --driver-memory 2g  --class ca.uwaterloo.cs451.cleaning.Sentiment target/assignments-1.0.jar
// 
// object Sentiment {
//   def main(args: Array[String]) {
//     
//     val spark = SparkSession.builder.appName("CS451 Project: Sentiment Analysis").getOrCreate()
//     val fs = FileSystem.get(spark.sparkContext.hadoopConfiguration)
//     val filename = "gme"
//     val gme = spark.read.format("csv").option("header", "true").load(filename + "/submissions_reddit.csv")
//       .selectExpr("created", "title")
//       .filter(_.getAs("created") != null)
//       .filter(_.getAs[String]("created").matches("^\\d\\d\\d\\d-\\d\\d-\\d\\d \\d\\d.*"))
//       .rdd
//       .map(row => (row.getAs[String]("created").slice(0, 13), row.getAs[String]("title")))
//       .map(row => {
//         (row._1, SentimentAnalysisUtils.detectSentiment(row._2))
//       })
//       // gme.foreach(println(_))
// 
//     fs.delete(new Path(filename+"_output"), true)
//     gme.saveAsTextFile(filename + "_output")
//     spark.stop()
//   }
// }
