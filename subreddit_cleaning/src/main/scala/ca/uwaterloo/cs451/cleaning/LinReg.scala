package ca.uwaterloo.cs451.cleaning

import org.apache.spark.sql.SparkSession
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.functions._
import org.apache.spark.ml.classification.LogisticRegression
import org.apache.spark.ml.feature.VectorAssembler
import org.apache.spark.mllib.evaluation.BinaryClassificationMetrics
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.ml.tuning.{CrossValidator, ParamGridBuilder}
import org.apache.spark.ml.evaluation.BinaryClassificationEvaluator

object LinReg {
  def getStocks(spark: SparkSession) : RDD[(String, Int)] = {
    spark.read.format("csv").option("header", "true").load("../data/gme-stocks")
      .selectExpr("Datetime", "Open", "Close")
      .rdd
      .map(row => {
        val date = row.getAs[String]("Datetime").slice(0, 13)
        val close = row.getAs[String]("Close").toFloat
        val open = row.getAs[String]("Open").toFloat
        (date, if (close > open) 1 else 0)
      })
      .filter(tuple => tuple._1 >= "2021-01-26 14" && tuple._1 <= "2021-04-15 15")
  }

  def clean_subreddit(spark: SparkSession, name: String, keywords: Array[String]) : RDD[(String, Float)] = {
    spark.read.format("csv").option("header", "true").load(s"../${name}/submissions_reddit.csv")
      .selectExpr("created", "title")
      .filter(_.getAs("created") != null)
      .filter(_.getAs[String]("created").matches("^\\d\\d\\d\\d-\\d\\d-\\d\\d \\d\\d.*"))
      .rdd
      .map(row => (row.getAs[String]("created").slice(0, 13), row.getAs[String]("title")))
      .filter(tuple => tuple._1 >= "2021-01-26 14" && tuple._1 <= "2021-04-15 15")
      .map(row => {
        val title = row._2.toLowerCase()
        var contains = false
        for (keyword <- keywords) {
          if (title.contains(keyword)) {
            contains = true
          }
        }
        (row._1, (if (contains) 1 else 0, 1))
      })
      .reduceByKey((p1, p2) => {
        (p1._1 + p2._1, p1._2 + p2._2)
      })
      .map(tuple => (tuple._1, tuple._2._1.toFloat / tuple._2._2.toFloat))
  }

  def main(args: Array[String]) {
    val spark = SparkSession.builder.appName("LinReg").getOrCreate()
    val gme = spark.createDataFrame(getStocks(spark)).toDF("date", "stock_inc")
    val keywords = Array("gme", "gamestop")
    val stocks = spark.createDataFrame(clean_subreddit(spark, "stocks", keywords)).toDF("date", "stocks_ratio")
    val wallstreetbets = spark.createDataFrame(clean_subreddit(spark, "wallstreetbets", keywords)).toDF("date", "wallstreetbets_ratio")

    var df = gme.join(stocks, "date")
      .join(wallstreetbets, "date")

    df = new VectorAssembler()
      .setInputCols(Array("stocks_ratio", "wallstreetbets_ratio"))
      .setOutputCol("features")
      .transform(df)
      .withColumnRenamed("stock_inc", "label")

    val seed = 5043
    val Array(train, test) = df.randomSplit(Array(0.7, 0.3), seed)
    val logisticRegression = new LogisticRegression()
      .setMaxIter(20)
      .setRegParam(0.02)
      .setElasticNetParam(0.8)
    val paramGrid = new ParamGridBuilder()
      .addGrid(logisticRegression.regParam, Array(0.1, 0.01))
      .build()
    val cv = new CrossValidator()
      .setEstimator(logisticRegression)
      .setEvaluator(new BinaryClassificationEvaluator)
      .setEstimatorParamMaps(paramGrid)
      .setNumFolds(3)
      .setParallelism(2)
    val model = cv.fit(train)

    val predictions = model.transform(test)
    val results = predictions
      .withColumn("correct", (predictions("label") === predictions("prediction")).cast("integer"))
      .withColumn("total", lit(1))
    results.selectExpr("date", "prediction", "label").show(100)
    results.agg(sum("correct"), sum("total")).show(1)

    spark.stop()
  }
}
