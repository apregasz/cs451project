import org.apache.spark.sql.SparkSession
import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import org.apache.hadoop.fs._
import scala.collection.JavaConverters._

object StockParser {
  def main(args: Array[String]) {
    val logFile = "README.md"
    val spark = SparkSession.builder.appName("CS451 Project: Stock data parser").getOrCreate()


    val stocks = spark.read.format("csv").option("header", "true").load("data/gme-stocks")
    .selectExpr("Datetime", "Open", "Close")
    .rdd
    .map(row => {
      val date = row.getAs[String]("Datetime").slice(0, 13)
      val close = row.getAs[String]("Close").toFloat
      val open = row.getAs[String]("Open").toFloat
      if (close > open) {
        (date, 1)
      } else {
        (date, 0)
      }
    })

    val outputDir = new Path("stock-data")
    FileSystem.get(spark.sparkContext.hadoopConfiguration).delete(outputDir, true)
    stocks.saveAsTextFile("stock-data")
    // println(stocks.collect()(0))
    spark.stop()
  }
}
